> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Justin Torres Ortega

### Assignment #1 Requirements:

*Sub-Heading:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Chapter Questions (CH. 1-2)

#### README.md file should include the following items:

* Screenshot of AMPPS Installation
* Screenshot running Java Hello
* Screenshot running Andriod Studio
* git commands w/ short description
* bitbucket rep links

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init: The git init command creates a new Git repository
2. git status: The git status command shows the current condition of the working directory and staging directory.
3. git add: The git add command adds a modification to the staging area from the working directory. It instructs Git to include changes to a certain file in the next commit.
4. git commit: git commit takes a snapshot of the project's current staged modifications.
5. git push: The git push command is used to transfer content from a local repository to a remote repository. Pushing is the process of sending commits from a local repository to a remote repository.
6. git pull: The git pull command fetches and downloads content from a remote repository and updates the local repository to match it.
7. git clean: git clean is a command that deletes untracked files from a repository's working directory.


#### Assignment Screenshots:

*Screenshot of PHP & MySql running *:

![Screenshot of PHP & MySql running](img/sql_php.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/hello_java.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/first_app.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
