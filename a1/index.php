<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
		<meta name="author" content="Justin Ortega">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment1</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		<style>
		
					html
{
	height: 100%;
}
	body 
{
	background-image: linear-gradient(#AF83E1 #E6F7FF, #FFD5FF);
	background-image: linear-gradient(to right, #AF83E1, #E6F7FF, #FFD5FF);
}

		</style>
		
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong><ol class="text-left">
						<li>Distributed Version Control with Git and Bitbucket</li>
						<li>Development Installations</li></p>

				<h4>Java Installation</h4>
				<img src="img/hello_java.png" class="img-responsive center-block" alt="JDK Installation">

				<h4>Android Studio Installation</h4>
				<img src="img/first_app.png" class="img-responsive center-block" alt="Android Studio Installation">

				<h4>AMPPS Installation</h4>
				<img src="img/sql_php.png" class="img-responsive center-block" alt="AMPPS Installation">
				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
