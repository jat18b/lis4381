
# LIS4381 Mobile Web Application Development

## Justin Torres Ortega


*Course skill sets*

1. Skill sets numbers 1-3
    - even_odd
    - Largest_Number
    - Arrays and Loops

2. Skill sets numbers 4-6
    - Decision Structures
    - Random Number Generator
    - Methods

#### README.md file should include the following items:

* Screenshot of Skill Set #1 Even or Odd
* Screenshot of Skill Set #2 Largest Number
* Screenshot of Skill Set #3 Arrays and Loops
* Screenshot of Skill Set #4 Decision Structures
* Screenshot of Skill Set #5 Random Number Generator
* Screenshot of Skill Set #6 Methods
* Screenshot of Skill Set #7 Random Number Generator Data Validation
* Screenshot of Skill Set #8 Largest of Three Numbers
* Screenshot of Skill Set #9 Array Runtime Data Validation
* Screenshot of Skill Set #10 Array List
* Screenshot of Skill Set #12 Temperature Calculator
* Screenshot of Skill Set #13 Sphere Volume Calculator
* Screenshot of Skill Set #14 Simple Calculator
* Screenshot of Skill Set #15 Write/Read File


#### Assignment Screenshots:

*Screenshot of skill set #1*:

![*Even or Odd*](img/larg_num.png)

*Screenshot of skill set #2*:

![*Largest Number*](img/evn_odd.png)

*Screenshot of skill set #3*:

![*Arrays and Loops*](img/arry_lps.png)

*Screenshot of skill set #4*:

![*Decision Structures*](img/des_stru.png)

*Screenshot of skill set #5*:

![*Random Numb Gen*](img/ran_num.png)

*Screenshot of skill set #6*:

![*Methods*](img/methods.png)

*Screenshot of skill set #7*:

![*Random Numb Data Validator*](img/data_validator.png)

*Screenshot of skill set #8*:

![*Largest of Three*](img/larg_o_three.png)

*Screenshot of skill set #9*:

![*Arrays Data Validator*](img/array_validator.png)

*Screenshot of skill set #10*:

![*Array List*](img/ss10.png)

*Screenshot of skill set #12*:

![*Temperature Calculator*](img/ss12.png)

*Screenshot of skill set #13*:

![*Sphere Program*](img/ss13.png)

#### PHP SkillSet 14:
| Screenshot of Index| Screenshot of Addition |
| -----------------------------------| ----------------------------------- |
| ![Screenshot of Index](img/ss14.png) | ![Screenshot of Addition](img/addition.png) |

| Screenshot of Index| Screenshot of Failed Division|
| -----------------------------------| ----------------------------------- |
| ![Screenshot of Index ](img/ss14_1.png) | ![Screenshot of Failed Division](img/division.png) |

#### PHP SkillSet 15:
| Screenshot of Index| Screenshot of Read/Write|
| -----------------------------------| ----------------------------------- |
| ![Screenshot of Index ](img/ss15.png) | ![Screenshot of Read/Write](img/read_write.png) |




*Link to Skill Set 1-15*
[Click here](https://bitbucket.org/jat18b/lis4381/src/master/skill_sets/)










