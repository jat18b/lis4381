> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Justin Torres Ortega

### Assignment #3 Requirements:

*Part 1*

1. Screenshot of Concert Ticket Application user interface
2. Screenshot of Concert Ticket Application processing user input
3. Screenshot of ERD
4. Screenshots of 10 records for each table
5. Links to a3.mwb and a3.sql file

*Part 2*

1. Skill Sets 4-6


#### README.md file should include the following items:

* Screenshot of running application's First user interface
* Screenshot of application processing user input
* Screenshot of ERD & 10 records for each table

#### Assignment Screenshots:
| Screenshot 1 of First user interface | Screenshot 2 of processing user input |
| -----------------------------------| ----------------------------------- |
| ![Android Studio first user Screenshot 1](img/a3_p1.png) | ![Android Studio second user Screenshot 2](img/a3_p2.png) |



##### Screenshot of Pet Store ERD
![Pet Store ERD](img/a3_erd.png)

#### Screenshot of 10 records from each Pet Store table

*Pet Table*
![Records from Pet Table](img/a3_pet.png)

*Pet Store Table*
![Records from Pet Store Table](img/a3_pstore.png)

*Customer Table*
![Records from Customer Table](img/a3_cus.png)

#### Links to a3.mwb & a3.sql files

[a3.sql file](https://bitbucket.org/jat18b/lis4381/src/master/a3/docs/a3.sql)

[a3.mwb file](https://bitbucket.org/jat18b/lis4381/src/master/a3/docs/a3.mwb)

##### Link to Skill Set 4-6

[click here](https://bitbucket.org/jat18b/lis4381/src/master/skill_sets/)


