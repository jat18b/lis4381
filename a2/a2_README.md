> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Justin Torres Ortega

### Assignment #2 Requirements:

*Part 1*

1. Screenshot of Android Studio "Healthy Recipies"
2. Change background color

*Part 2*

1. Skill Sets 1-3


#### README.md file should include the following items:

* Screenshot of running application's First user interfaace
* Screenshot of running application's Second user interface

#### Assignment Screenshots:

*Screenshot of "Healthy Recipies" First user interface*:

![*Screenshot of "Healthy Recipies" U:1*](img/U1.png)

*Screenshot of "Healthy Recipies" Second user interface*:

![*Screenshot of "Healthy Recipies" U:2*](img/U2.png)




