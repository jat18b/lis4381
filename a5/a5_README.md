> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Justin Torres Ortega

### Assignment #5 Requirements:

*Part 1*

1. Manage Local Host database to perform server side validation using JSON and Bootstrap
2. Use jQuery to validate client side data
3. Link to local host web application

*Part 2*

1. Skill Sets 13-15


#### README.md file should include the following items:

* Screenshot of Index.php
* Screenshot of Correct Data Validation
* Screenshot of Failed Data Vlidation
* Screenshot of Client side Data Validation
* Screenshot of Insertion running on local host

##### Screenshot of Online Portfolio Main Page
![Main Page](img/imain.png)


#### Assignment Screenshots:
| Screenshot of Correct Data Validation | Screenshot of Failed Data Vlidation |
| -----------------------------------| ----------------------------------- |
| ![Screenshot of Correct Data Validation ](img/working.png) | ![Screenshot of Failed Data Vlidation](img/failed.png) |

#### Assignment Screenshots:
| Screenshot of Client side Data Validation| Insertion running on local host |
| -----------------------------------| ----------------------------------- |
| ![Screenshot of Client side Data Validation ](img/datval.png) | ![Insertion running on local host](img/insert_run.png) |



##### Link to Skill Set 13-15

[click here](https://bitbucket.org/jat18b/lis4381/src/master/skill_sets/)


