> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Justin Torres Ortega

### Project #1 Requirements:

*Part 1*

1.Create My Buisness Card App using Andriod Studio
2.Include personal Launcher Icon, Background-Colors, and Images


*Part 2*

1.Skill sets 7-9 


#### README.md file should include the following items:

* Course title, your name, assignment requirements, as per A1; 
* Screenshot of running application's first user interface; 
* Screenshot of running application's second user interface; 


#### Assignment Screenshots:


*Screenshot of P1 My Business Card*:

| Screenshot 1 |  Screenshot 2 |
| -----------------------------------| ----------------------------------- |
| ![P1 Screenshot 1](img/p1.png) | ![P2 Screenshot 2](img/p2.png) |

##### Link to Skill Set 7-9

[click here](https://bitbucket.org/jat18b/lis4381/src/master/skill_sets/)


