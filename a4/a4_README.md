> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Justin Torres Ortega

### Assignment #4 Requirements:

*Part 1*

1. Create an online portfolio web app
2. Link A1-P2 to ensure all links are functional
3. Screenshots of Online Portfoliio

*Part 2*

1. Skill Sets 10-12


#### README.md file should include the following items:

* Screenshot of Online Portfolio Main Page
* Screenshot of Working Insert Validation
* Screenshot of Failed Insert Vlidation

##### Screenshot of Online Portfolio Main Page
![Main Page](img/main.png)


#### Assignment Screenshots:
| Screenshot of Working Inserts | Screenshot of Failed Inserts |
| -----------------------------------| ----------------------------------- |
| ![Screenshot of Working Inserts ](img/working.png) | ![Screenshot of Failed Inserts](img/failed.png) |



##### Link to Skill Set 10-12

[click here](https://bitbucket.org/jat18b/lis4381/src/master/skill_sets/)


