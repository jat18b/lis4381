> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Justin Torres Ortega

### Project #2 Requirements:


*Parts*

1. Update previous Assignment 4 and 5 to continue C.R.U.D. protocol.
2. Project 2 works on Update and Deleting Records from client database
3. Includes RSS Feed of creators chosing.


#### Assignment Screenshots:



### Landing Page and Screenshot of P2 Index.php:

| Landing Page|  Index.php | RSS Feed |
| -----------------------------------| ----------------------------------- | ----------------------------------- |
| ![Landing Page](img/index.png) | ![Index.php](img/p2.png) | ![RSS Feed](img/rss.png) |

### Edit Petstore

| Pre Update | Edit Row |
| ----------------------------------- | ----------------------------------- |
| ![Pre Update](img/preupdate.png) | ![Update Edit](img/afupdate.png) |


|  Faild Validation | Passed Updated |
| ----------------------------------- | ----------------------------------- |
| ![Update Error](img/fail_invalid.png) | ![Update Complete](img/complete.png) |


### Delete Petstore
|Pre Delete | Delete Record |  Sucessful Deletetion |
| -----------------------------------| ----------------------------------- | ----------------------------------- |
|  ![PreDelete](img/pre_delete.png) |![Mid Delete](img/al_delete.png) | ![Post Delete](img/af_delete.png) |