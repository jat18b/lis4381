> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS4381 Mobile Web Application Development

## Justin Torres Ortega

### Class Number Requirements:

*Course Work Links:*

1. [A1 README.md](a1/a1_README.md "My A1 README.md file")
    - Install Homebrew.Bash (mysql for mac)
    - Install JDK
    - Install Andriod Studio and Create My First APP
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorial (bitbucketstationlocations)
    - Provide git commands descriptions

2. [A2 README.md](a2/a2_README.md "My A2 README.md file")
    - Create Healthy Recipies Application
    - Successfully Run Application using Android Studio
    - Skill Sets 1-3 [SS README.md](skill_sets/SS_README.md "My Skill Sets README.md file")
    - Update Bitbucket Repository

3. [A3 README.md](a3/a3_README.md "My A3 README.md file")
    - Screenshot of Concert Ticket Application user interface
    - Screenshot of Concert Ticket Application processing user input
    - Screenshot of ERD
    - Screenshots of 10 records for each table
    - Links to a3.mwb and a3.sql file
    - Skill Sets 4-6 [SS README.md](skill_sets/SS_README.md "My Skill Sets README.md file")

4. [P1 README.md](p1/p1_README.md "My P1 README.md file")
    - Create My Buisness Card App using Andriod Studio
    - Include personal Launcher Icon, Background-Colors, and Images
    - Skill sets 7-9 [SS README.md](skill_sets/SS_README.md "My Skill Sets README.md file")

5. [A4 README.md](a4/a4_README.md "My A4 README.md file")
    - Manage Online portfolio using PHP
    - A1-P2 working portfolio links
    - Screenshots of 4381 Web Apps
    - Skill sets 10-12 [SS README.md](skill_sets/SS_README.md "My Skill Sets README.md file")

6. [A5 README.md](a5/a5_README.md "My A5 README.md file")
    - Manage Local Host database to perform server side validation using JSON and Bootstrap
    - Use jQuery to validate client side data
    - Link to local host web application
    - Skill Sets 13-15 [SS README.md](skill_sets/SS_README.md "My Skill Sets README.md file")

7. [P2 README.md](p2/p2_README.md "My P2 README.md file")
    - Manage Local Host database to perform server side validation using JSON and Bootstrap
    - Project 2 works on Update and Deleting Records from client database
    - RSS Feed of my choosing

### *Tables*: 
*Add table: use three or more hyphens (---) to create each column's header, and use pipes (|) to seperate each column. (optionally add pipes on either end of the table.)*
>
| Syntax    | Description |
| ----------| ----------- |
| Header    | Title       |
| Paragraph | Text        |
>
>
### Alignment: 
*Align text: left, right, or center by adding a colon (:) to left, right, or on both sides of hyphens with the header row*
>
| Syntax    | Description |  Example  |
| :---      |  :----:     |         ---: |
| left     | Center      | Right    |
>

